import { HsmlFragment } from "peryl/dist/hsml";

export function hello(name: string): HsmlFragment {
    return [
        ["p", [
            ["input.w3-input", { type: "text", value: name }],
            ["p", [
                "Hello ", ["strong", [name]], " !"
            ]]
        ]]
    ];
}
