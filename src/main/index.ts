import * as fs from "fs";
import * as path from "path";
import { HsmlFragment } from "peryl/dist/hsml";
import { hsmls2htmls } from "peryl/dist/hsml-html";
import { page } from "./components/page";
import { appshell } from "./components/appshell";
import { sidebar } from "./components/sidebar";
import { content } from "./components/content";

function HTML(file: string, hsmls: HsmlFragment, pretty = false): void {
    const p = path.join(__dirname, "..", "..", "dist", file);
    console.log("generate:", p);
    const html = hsmls2htmls(hsmls, pretty).join("");
    fs.writeFileSync(p, html);
}


const pretty = true;

const siteTitle = "Patrick";

let file = "index.html";
HTML(file,
    page(siteTitle,
        appshell(siteTitle, "Index",
            sidebar(file),
            content("Index Title", "name index")
        )
    ),
    pretty
);

file = "overview.html";
HTML(file,
    page(siteTitle,
        appshell(siteTitle, "Overview",
            sidebar(file),
            content("Overview Title", "name overview")
        )
    ),
    pretty
);

file = "views.html";
HTML(file,
    page(siteTitle,
        appshell(siteTitle, "Views",
            sidebar(file),
            content("Views Title", "name views")
        )
    ),
    pretty
);

file = "news.html";
HTML(file,
    page(siteTitle,
        appshell(siteTitle, "News",
            sidebar(file),
            content("News Title", "name news")
        )
    ),
    pretty
);

file = "settings.html";
HTML(file,
    page(siteTitle,
        appshell(siteTitle, "Settings",
            sidebar(file),
            content("Settings Title", "name settings")
        )
    ),
    pretty
);
